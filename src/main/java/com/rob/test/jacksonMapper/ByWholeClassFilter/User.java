package com.rob.test.jacksonMapper.ByWholeClassFilter;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.HashMap;
import java.util.List;

/**
 * Created by rob on 29/10/15.
 */

@JsonFilter("myFilter")
public class User {



    private String name;
    private Integer id;

    private HashMap<String, Object> other;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public HashMap<String, Object> getOther() {
        return other;
    }

    public void setOther(HashMap<String, Object> other) {
        this.other = other;
    }
}
