package com.rob.test.jacksonMapper.ByWholeClassFilter;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.BeanPropertyWriter;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.PropertyFilter;
import com.fasterxml.jackson.databind.ser.PropertyWriter;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

import java.util.HashMap;

/**
 * Created by rob on 29/10/15.
 */
public class MainSerializeWholeClass {

    public static void main(String[] args) throws JsonProcessingException {

        new MainSerializeWholeClass().run();
    }




    private void run() throws JsonProcessingException {

        PropertyFilter theFilter = new SimpleBeanPropertyFilter() {
            @Override
            public void serializeAsField
                    (Object pojo, JsonGenerator jgen, SerializerProvider provider, PropertyWriter writer)
                    throws Exception {
                if (include(writer)) {
                    if (!writer.getName().equals("other")) {
                        writer.serializeAsField(pojo, jgen, provider);
                        return;
                    }
                    HashMap<String, Object> others = ((User) pojo).getOther();
                    for(String key : others.keySet()){
                        jgen.writeObjectField(key, others.get(key));
                    }

                    //writer.serializeAsField(pojo, jgen, provider);

                } else if (!jgen.canOmitFields()) { // since 2.3
                    writer.serializeAsOmittedField(pojo, jgen, provider);
                }
            }
            @Override
            protected boolean include(BeanPropertyWriter writer) {
                return true;
            }
            @Override
            protected boolean include(PropertyWriter writer) {
                return true;
            }
        };

        final User user = new User();

        user.setId(123);
        user.setName("rob");

        HashMap<String, Object> otherParams = new HashMap<>();
        otherParams.put("some value", 123445);
        otherParams.put("bla bla", "23452345");
        user.setOther(otherParams);

        ObjectMapper mapper = new ObjectMapper();

//        String jsonInString = mapper.writeValueAsString(user);
//
//        System.out.println("\nWITHOUT  custom conversion\n\n");
//        System.out.println(jsonInString);





        FilterProvider filters = new SimpleFilterProvider().addFilter("myFilter", theFilter);


        String dtoAsString = mapper.writer(filters).writeValueAsString(user);

        System.out.println(dtoAsString);


    }
}
