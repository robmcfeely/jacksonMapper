package com.rob.test.jacksonMapper.com.rob.test.jacksonMapper.CustomOtherFieldAnnotation;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

/**
 * Created by rob on 03/11/15.  Automated based ONLY on  @JsonAnyGetter and  @JsonAnySetter
 */
public class MainDoByUnknownFieldsSerializeAndDeserialize {

    public static void main(String[] args) throws IOException {

        ObjectMapper objectMapper = new ObjectMapper();

        MyClass myClass2 = new MyClass();
        myClass2.putDynamicField("other-property", 1234);
        myClass2.putDynamicField("hold a chicken in the air", "stick a deck chair up your nose");
        myClass2.setProperty("John");

        String json = objectMapper.writeValueAsString(myClass2);

        System.out.println(json);

        MyClass mClass4 = objectMapper.readValue(json, MyClass.class);

        System.out.println("Deserialized");
        System.out.println(mClass4.getProperty());
        final Set<Map.Entry<String, Object>> entries = mClass4.getDynamicFields().entrySet();
        for(Map.Entry<String, Object> entry : entries){
            System.out.println(entry.getKey()+" : "+entry.getValue());
        }



    }


}
