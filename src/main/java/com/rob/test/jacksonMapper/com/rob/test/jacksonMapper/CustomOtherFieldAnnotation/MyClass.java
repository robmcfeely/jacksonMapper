package com.rob.test.jacksonMapper.com.rob.test.jacksonMapper.CustomOtherFieldAnnotation;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by rob on 03/11/15.
 * Class that supprts a bag of unknown properties that is transferrable to a hashmap
 */
public class MyClass extends DynamicFields{

    private String property;

    private Map<String, Object> other = new HashMap<String, Object>();

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }
//
//    @JsonAnyGetter
//    public Map<String, Object> getOther() {
//        return other;
//    }
//
//    @JsonAnySetter
//    public void setOther(String name, Object value) {
//        other.put(name, value);
//    }



}
