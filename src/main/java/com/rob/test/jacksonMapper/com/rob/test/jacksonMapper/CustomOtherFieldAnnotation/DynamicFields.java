package com.rob.test.jacksonMapper.com.rob.test.jacksonMapper.CustomOtherFieldAnnotation;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;

import java.util.HashMap;

/**
 * Created by rob on 09/12/15.
 * This class is to be used to encapsulate dynamic fields which can be serialized as first order json key values usign the jackson anyGetter, anySetter  Extending it is a good option as its accessors are protected
 */
public class DynamicFields {

    private HashMap<String, Object> dynamicFields = new HashMap<>();

    @JsonAnySetter
    public final void putDynamicField(String key, Object value){
        dynamicFields.put(decapitalizeFirstLetter(key), value);
    }

    @JsonAnyGetter
    public final HashMap<String, Object> getDynamicFields(){
        return dynamicFields;
    }

    private String decapitalizeFirstLetter(String string){
        if (string == null || string.length() == 0) {
            return string;
        }
        char c[] = string.toCharArray();
        c[0] = Character.toLowerCase(c[0]);
        return new String(c);
    }

}
