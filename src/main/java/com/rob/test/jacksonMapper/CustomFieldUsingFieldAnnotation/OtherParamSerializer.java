package com.rob.test.jacksonMapper.CustomFieldUsingFieldAnnotation;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by rob on 03/11/15.  Designed to serialize a hashmap to a flat set of fields
 */
public class OtherParamSerializer extends JsonSerializer<HashMap<String, Object>> {
    @Override
    public void serialize(HashMap<String, Object> others, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {

        for(String key : others.keySet()){
            jgen.writeObjectField(key, others.get(key));
        }
    }
}
