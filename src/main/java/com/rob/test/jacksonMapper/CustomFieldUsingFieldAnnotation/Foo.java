package com.rob.test.jacksonMapper.CustomFieldUsingFieldAnnotation;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.HashMap;

/**
 * Created by rob on 03/11/15.
 */
public class Foo {


        private String name;
        private Integer id;

        @JsonSerialize(using = OtherParamSerializer.class)
        private HashMap<String, Object> other;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }


        public HashMap<String, Object> getOther() {
            return other;
        }

        public void setOther(HashMap<String, Object> other) {
            this.other = other;
        }
    }

