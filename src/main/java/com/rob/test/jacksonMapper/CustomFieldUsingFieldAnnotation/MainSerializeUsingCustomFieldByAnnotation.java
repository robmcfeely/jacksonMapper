package com.rob.test.jacksonMapper.CustomFieldUsingFieldAnnotation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;

/**
 * Created by rob on 03/11/15.
 */
public class MainSerializeUsingCustomFieldByAnnotation {

    public static void main(String[] args) throws JsonProcessingException {
        final Foo user = new Foo();

        user.setId(123);
        user.setName("foo");

        HashMap<String, Object> otherParams = new HashMap<>();
        otherParams.put("some foo", 123445);
        otherParams.put("foo foo", "23452345");
        user.setOther(otherParams);

        ObjectMapper mapper = new ObjectMapper();
        System.out.println(mapper.writeValueAsString(user));


    }
}
